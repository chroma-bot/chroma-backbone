package main

import "errors"

func AddShard(id string, dsid int32, pid int32, st ShardState) *StateAction {
	return &StateAction{
		Name: "ADD_SHARD",
		Data: Shard{
			ID:             id,
			DiscordShardID: dsid,
			GuildIDs:       []string{},
			PID:            pid,
			State:          st,
		},
		ActionCompleted: make(chan bool),
	}
}

func RemoveShard(id string) *StateAction {
	return &StateAction{
		Name:            "REMOVE_SHARD",
		Data:            id,
		ActionCompleted: make(chan bool),
	}
}

func TakeShard(num int32) *StateAction {
	return &StateAction{
		Name:            "TAKE_SHARD",
		Data:            num,
		ActionCompleted: make(chan bool),
	}
}

func ReleaseShard(num int32) *StateAction {
	return &StateAction{
		Name:            "RELEASE_SHARD",
		Data:            num,
		ActionCompleted: make(chan bool),
	}
}

func SafeTakeNextShard(state *SyncState) (int32, error) {
	st := state.State()
	if len(st.FreeShardIds) == 0 {
		return 0, errors.New("No free shards available")
	}
	state.Dispatch(TakeShard(st.FreeShardIds[0]))
	return st.FreeShardIds[0], nil
}

type UpdateShardStateData struct {
	ID    string
	State ShardState
}

func UpdateShardState(id string, st ShardState) *StateAction {
	return &StateAction{
		Name: "UPDATE_SHARD_STATE",
		Data: UpdateShardStateData{
			ID:    id,
			State: st,
		},
		ActionCompleted: make(chan bool),
	}
}
