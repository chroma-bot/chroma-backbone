package main

import (
	"time"

	"github.com/alecthomas/kingpin"
	"github.com/sirupsen/logrus"
)

var (
	verbose    = kingpin.Flag("verbose", "Enable higher logging detail").Short('v').Bool()
	configPath = kingpin.Arg("config", "Path to load configuration from").Default("./config.toml").String()
)

func main() {
	launchTime := time.Now()

	kingpin.Parse()

	if *verbose {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.Debug("Logging verbosely")
	}
	logrus.Info("Starting...")
	logrus.Debugf("Loading config from %s", *configPath)
	conf, err := LoadConfig(*configPath)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Debug("Opening websocket server")
	ws := NewServer(conf)
	ws.Run(launchTime)
}
