package main

import (
	"errors"

	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"

	"github.com/golang/protobuf/proto"
	cproto "gitlab.com/chroma-bot/chroma-protobuf"
)

func (s *Server) handleHandshake(c *websocket.Conn) error {
	msg, err := ProtoMessage(cproto.Message_HELLO, []byte{})
	if err != nil {
		return err
	}

	logrus.Debug("Writing HELLO to the websocket")
	err = c.WriteMessage(websocket.BinaryMessage, msg)
	if err != nil {
		return err
	}

	logrus.Debug("Listening for a response...")
	_, m, err := c.ReadMessage()
	if err != nil {
		return err
	}

	recvMsg, err := ProtoMessageUnmarshal(m)
	if err != nil {
		return err
	}

	logrus.Debugf("Got message with type %s", cproto.Message_MessageType_name[int32(recvMsg.Type)])
	if recvMsg.Type != cproto.Message_HS_INIT {
		return errors.New("Message was not HS_INIT")
	}

	hs_init := cproto.HandshakeInit{}
	proto.Unmarshal(recvMsg.Payload, &hs_init)

	// TODO verify version

	switch hs_init.Type {
	case cproto.HandshakeInit_SHARD:
		logrus.Debug("Connected client identifying as a shard")
		logrus.Debug("Taking shard ID from state")
		id, err := SafeTakeNextShard(s.State)
		if err != nil {
			return err
		}
		logrus.Debugf("Free ID is %d", id)
		uid := uuid.NewV4()
		hsr := &cproto.ShardHandshakeResponse{
			Id:         uid.String(),
			ShardId:    id,
			ShardCount: s.Config.Backbone.MaxShards,
		}
		hsr_ser, err := proto.Marshal(hsr)
		if err != nil {
			return err
		}
		msg, err = ProtoMessage(cproto.Message_SHARD_HS_RESP, hsr_ser)
		if err != nil {
			return err
		}
		logrus.Debug("Writing response to websocket")
		c.WriteMessage(websocket.BinaryMessage, msg)
		logrus.Debug("Writing shard into state")
		s.State.Dispatch(AddShard(uid.String(), id, hs_init.Pid, SHARD_ONLINE))
		logrus.Infof("Shard %s completed handshake successfully.", uid.String())
		s.listenShard(c, uid.String())
		logrus.Debugf("Shard %s disconnected, releasing ID.", uid.String())
		s.State.Dispatch(ReleaseShard(id))
		s.State.Dispatch(RemoveShard(uid.String()))
	default:
		return errors.New("Client type currently unsupported")
	}

	return nil
}
