package main

func BackboneReducer(s BackboneState, a *StateAction) BackboneState {
	switch a.Name {
	case "ADD_SHARD": // does this *have* to be immutable?
		// copy shards map
		nShards := map[string]Shard{}
		for k, v := range s.Shards {
			nShards[k] = v
		}
		nShards[a.Data.(Shard).ID] = a.Data.(Shard)

		return BackboneState{Shards: nShards, FreeShardIds: s.FreeShardIds}
	case "REMOVE_SHARD": // does this *have* to be immutable?
		// copy shards map
		nShards := map[string]Shard{}
		for k, v := range s.Shards {
			if v.ID != a.Data.(string) {
				nShards[k] = v
			}
		}

		return BackboneState{Shards: nShards, FreeShardIds: s.FreeShardIds}
	case "TAKE_SHARD":
		num := a.Data.(int32)
		nShards := []int32{}
		for _, n := range s.FreeShardIds {
			if n != num {
				nShards = append(nShards, n)
			}
		}
		return BackboneState{Shards: s.Shards, FreeShardIds: nShards}
	case "RELEASE_SHARD":
		return BackboneState{Shards: s.Shards, FreeShardIds: append(s.FreeShardIds, a.Data.(int32))}
	case "UPDATE_SHARD_STATE": // does this *have* to be immutable?
		// copy shards map
		nShards := map[string]Shard{}
		for k, v := range s.Shards {
			nShards[k] = v
		}
		oshard := nShards[a.Data.(UpdateShardStateData).ID]
		nShards[a.Data.(UpdateShardStateData).ID] = Shard{
			ID:             oshard.ID,
			DiscordShardID: oshard.DiscordShardID,
			GuildIDs:       oshard.GuildIDs,
			PID:            oshard.PID,
			State:          a.Data.(UpdateShardStateData).State,
		}

		return BackboneState{Shards: nShards, FreeShardIds: s.FreeShardIds}

	default:
		panic("undefined state action " + a.Name)
	}
}
