package main

import (
	"time"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"

	cproto "gitlab.com/chroma-bot/chroma-protobuf"
)

func (s *Server) listenShard(ws *websocket.Conn, shardID string) {
	logrus.Debugf("Entered listen loop for shard %s", shardID)

	connClosed := false
	defer func() { connClosed = true }()

	hbWait := false
	go func() {
		hbsub := s.EventBus.Sub("heartbeat")

		for range hbsub {
			if connClosed {
				s.EventBus.Unsub(hbsub, "heartbeat")
				return
			}
			// TODO check if shard process is alive
			logrus.Debugf("Sending heartbeat on shard %s", shardID)
			msg, err := ProtoMessage(cproto.Message_HEARTBEAT, []byte{})
			if err != nil {
				logrus.Errorf("Failed to serialize heartbeat... %v", err)
			} else {
				ws.WriteMessage(websocket.BinaryMessage, msg)
				hbWait = true
				go func() {
					time.Sleep(time.Millisecond * time.Duration(s.Config.Backbone.HBTol))
					if hbWait && !connClosed {
						logrus.Warnf("Shard %s is not responding!", shardID)
						s.State.Dispatch(UpdateShardState(shardID, SHARD_UNRESPONSIVE))
					}
				}()
			}
		}
		logrus.Warnf("Heartbeat loop for %s exited early!", shardID)
	}()
	for {
		_, msg, err := ws.ReadMessage()
		if err != nil {
			logrus.Errorf("Error reading from websocket: %v", err)
			return
		}

		logrus.Debug("Got message over websocket (not showing binary data)")

		wm, err := ProtoMessageUnmarshal(msg)
		if err != nil {
			logrus.Errorf("Failed to deserialize message... %v", err)
			return
		}

		state := s.State.State().Shards[shardID]

		switch wm.Type {
		case cproto.Message_HEARTBEAT:
			logrus.Debug("Got heartbeat back.")
			hbWait = false
			if state.State == SHARD_UNRESPONSIVE {
				logrus.Infof("Shard %s came back.", shardID)
				s.State.Dispatch(UpdateShardState(shardID, SHARD_ONLINE))
			}
		default:
			logrus.Warnf("Shard %s sent unhandled message type %v", shardID, wm.Type) // TODO look up proper message name
		}
	}
}
