package main

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func webhookStateSub(cfg Config) StateSubscriber {
	session, _ := discordgo.New()
	return func(state BackboneState, action *StateAction) {
		switch action.Name {
		case "ADD_SHARD":
			a := action.Data.(Shard)
			session.WebhookExecute(cfg.Backbone.WebhookID, cfg.Backbone.WebhookToken, false, &discordgo.WebhookParams{
				Embeds: []*discordgo.MessageEmbed{
					&discordgo.MessageEmbed{
						Title:       "Shard connected",
						Description: fmt.Sprintf("Shard %d (%s) has come online.", a.DiscordShardID, a.ID),
					},
				},
			})
		case "REMOVE_SHARD":
			uid := action.Data.(string)
			s := state.Shards[uid]
			session.WebhookExecute(cfg.Backbone.WebhookID, cfg.Backbone.WebhookToken, false, &discordgo.WebhookParams{
				Embeds: []*discordgo.MessageEmbed{
					&discordgo.MessageEmbed{
						Title:       "Shard disconnected",
						Description: fmt.Sprintf("Shard %d (%s) has gone offline.", s.DiscordShardID, s.ID),
					},
				},
			})
		case "UPDATE_SHARD_STATE":
			dat := action.Data.(UpdateShardStateData)
			s := state.Shards[dat.ID]
			if dat.State == SHARD_UNRESPONSIVE {
				session.WebhookExecute(cfg.Backbone.WebhookID, cfg.Backbone.WebhookToken, false, &discordgo.WebhookParams{
					Embeds: []*discordgo.MessageEmbed{
						&discordgo.MessageEmbed{
							Title:       "Shard unresponsive",
							Description: fmt.Sprintf("Shard %d (%s) has gone unresponsive.", s.DiscordShardID, s.ID),
						},
					},
				})
			}
			if dat.State == SHARD_ONLINE {
				session.WebhookExecute(cfg.Backbone.WebhookID, cfg.Backbone.WebhookToken, false, &discordgo.WebhookParams{
					Embeds: []*discordgo.MessageEmbed{
						&discordgo.MessageEmbed{
							Title:       "Shard back online",
							Description: fmt.Sprintf("Shard %d (%s) has come back online.", s.DiscordShardID, s.ID),
						},
					},
				})
			}
		}
	}
}
