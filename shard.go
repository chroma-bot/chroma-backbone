package main

type ShardState int32

const (
	SHARD_ONLINE ShardState = iota
	SHARD_UNRESPONSIVE
	SHARD_OFFLINE
)

type Shard struct {
	ID             string
	DiscordShardID int32
	GuildIDs       []string
	PID            int32
	State          ShardState
}
