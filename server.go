package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/cskr/pubsub"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type Server struct {
	Config    Config
	upgrader  websocket.Upgrader
	State     *SyncState
	Heartbeat *time.Ticker
	EventBus  *pubsub.PubSub
}

func NewServer(conf Config) Server {
	logrus.Debugf("Starting server with %d max shards", conf.Backbone.MaxShards)
	st := NewSyncState(InitialBackboneState(conf.Backbone.MaxShards))
	st.Reducer(BackboneReducer)
	if conf.Backbone.EnableWebhook {
		st.PreSubscribe(webhookStateSub(conf))
	}
	return Server{
		Config:    conf,
		upgrader:  websocket.Upgrader{},
		State:     st,
		Heartbeat: time.NewTicker(time.Millisecond * time.Duration(conf.Backbone.HBRate)),
		EventBus:  pubsub.New(1),
	}
}

func (s *Server) handler(w http.ResponseWriter, r *http.Request) {
	logrus.Debugf("Upgrading new client from %s", r.Host)

	c, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		logrus.Errorf("Failed to upgrade client: %v", err)
		return
	}

	err = s.handleHandshake(c)
	if err != nil {
		logrus.Errorf("Failed to complete handshake with client: %v", err)
		c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(1011, err.Error()))
		return
	}
}

func (s *Server) Run(launchTime time.Time) {
	go func() {
		for {
			<-s.Heartbeat.C
			logrus.Debug("Emitting heartbeat")
			s.EventBus.Pub(nil, "heartbeat")
		}
	}()
	http.HandleFunc("/", s.handler)
	logrus.Infof("Starting server on %s:%s after %s", s.Config.Backbone.IP, s.Config.Backbone.Port, time.Since(launchTime))
	logrus.Fatal(http.ListenAndServe(fmt.Sprintf("%s:%s", s.Config.Backbone.IP, s.Config.Backbone.Port), nil))
}
