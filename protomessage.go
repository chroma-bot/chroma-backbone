package main

import (
	"github.com/golang/protobuf/proto"
	cproto "gitlab.com/chroma-bot/chroma-protobuf"
)

func ProtoMessage(mtype cproto.Message_MessageType, payload []byte) ([]byte, error) {
	msg := &cproto.Message{
		Type:    mtype,
		Payload: payload,
	}

	out, err := proto.Marshal(msg)
	if err != nil {
		return []byte{}, err
	}
	return out, nil
}

func ProtoMessageUnmarshal(m []byte) (cproto.Message, error) {
	msg := cproto.Message{}
	err := proto.Unmarshal(m, &msg)
	if err != nil {
		return cproto.Message{}, err
	}
	return msg, nil
}
