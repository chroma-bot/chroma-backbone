package main

////

type BackboneState struct {
	Shards       map[string]Shard
	FreeShardIds []int32
}

func InitialBackboneState(shardCount int32) BackboneState {
	fShards := []int32{}
	for i := int32(0); i < shardCount; i++ {
		fShards = append(fShards, i)
	}
	return BackboneState{
		Shards:       map[string]Shard{},
		FreeShardIds: fShards,
	}
}

////

type StateAction struct {
	Name            string
	Data            interface{}
	ActionCompleted chan bool
}

type StateReadOp struct {
	state chan BackboneState
}

type SyncState struct {
	actionQueue     chan *StateAction
	readQueue       chan *StateReadOp
	state           BackboneState
	preSubscribers  []StateSubscriber
	postSubscribers []StateSubscriber
}

type StateReducer func(BackboneState, *StateAction) BackboneState

type StateSubscriber func(BackboneState, *StateAction)

////

func NewSyncState(init BackboneState) *SyncState {
	return &SyncState{
		actionQueue:     make(chan *StateAction),
		readQueue:       make(chan *StateReadOp),
		state:           init,
		preSubscribers:  make([]StateSubscriber, 0),
		postSubscribers: make([]StateSubscriber, 0),
	}
}

func (s *SyncState) Reducer(reducer StateReducer) {
	go func() {
		for {
			select {
			case read := <-s.readQueue:
				read.state <- s.state
			case action := <-s.actionQueue:
				s.state = reducer(s.state, action)
				action.ActionCompleted <- true
			}
		}
	}()
}

func (s *SyncState) Dispatch(action *StateAction) {
	for _, sb := range s.preSubscribers {
		go sb(s.State(), action)
	}
	s.actionQueue <- action
	<-action.ActionCompleted
	for _, sb := range s.postSubscribers {
		go sb(s.State(), action)
	}
}

func (s *SyncState) State() BackboneState {
	op := StateReadOp{
		state: make(chan BackboneState),
	}
	s.readQueue <- &op
	state := <-op.state
	return state
}

func (s *SyncState) PreSubscribe(sb StateSubscriber) {
	s.preSubscribers = append(s.preSubscribers, sb)
}

func (s *SyncState) PostSubscribe(sb StateSubscriber) {
	s.postSubscribers = append(s.postSubscribers, sb)
}
