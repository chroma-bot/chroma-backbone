package main

import "github.com/BurntSushi/toml"

type BackboneConfig struct {
	IP            string
	Port          string
	MaxShards     int32  `toml:"max_shards"`
	HBRate        int32  `toml:"heartbeat_rate"`
	HBTol         int32  `toml:"heartbeat_tolerance"`
	EnableWebhook bool   `toml:"enable_webhook"`
	WebhookID     string `toml:"webhook_id"`
	WebhookToken  string `toml:"webhook_token"`
}

type Config struct {
	Backbone BackboneConfig
}

func LoadConfig(path string) (Config, error) {
	var conf Config
	_, err := toml.DecodeFile(path, &conf)
	if err != nil {
		return Config{}, err
	}
	return conf, nil
}
